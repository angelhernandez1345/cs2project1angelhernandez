package edu.westga.cs1302.nss.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.cs1302.nss.model.Earthquake;
import edu.westga.cs1302.nss.model.Network;
import edu.westga.cs1302.nss.model.Station;
import edu.westga.cs1302.nss.resources.GeneralConstants;
import edu.westga.cs1302.nss.resources.UI.ExceptionMessages;

/**
 * Reads seismic data from a file with extension Constants.FILE_EXTENSION which
 * is a CSV file with the following format:
 * station,year,month,day,hour,minute,second,location,magnitude,significance,distance,depth
 * 
 * @author CS1302
 * @version Fall 2021
 */
public class DataFileReader {

	private File seismicDataFile;

	/**
	 * Instantiates a new seismic data file reader.
	 *
	 * @precondition seismicDataFile != null
	 * @postcondition none
	 * @param seismicDataFile the seismic data file
	 */
	public DataFileReader(File seismicDataFile) {
		if (seismicDataFile == null) {
			throw new IllegalArgumentException(ExceptionMessages.FILE_CANNOT_BE_NULL);
		}
		this.seismicDataFile = seismicDataFile;
	}

	/**
	 * Reads all the earthquake data from the file one line at a time where each
	 * line contains information about the earthquake as well as the station where
	 * it was recorded. Parses each line and creates an earthquake object and stores
	 * it in the corresponding station of the passed in network.
	 * 
	 * @precondition network != null
	 * @postcondition none
	 * @param network the network to load from file
	 * @return the list of names of all newly added stations
	 * @throws FileNotFoundException exception to be thrown if file is not found
	 */
	public ArrayList<String> loadAllEarthquakesToStations(Network network) throws FileNotFoundException {
		if (network == null) {
			throw new IllegalArgumentException(ExceptionMessages.NETWORK_CANNOT_BE_NULL);
		}
		
		ArrayList<String> stations = new ArrayList<String>();
		int lineNumber = 1;
		String lineContent = null;
		
		try (Scanner inFile = new Scanner(this.seismicDataFile)) {
		
			while (inFile.hasNextLine()) {
				try {
					String currentLine = inFile.nextLine();
					lineContent = currentLine;
					String [] element = currentLine.split(GeneralConstants.FIELD_SEPARATOR); 
					String stationName = element[0];
					String yearString = element[1];
					String monthString = element[2];
					String dayString = element[3];
					String hourString = element[4];
					String minuteString = element[5];
					String secondsString = element[6];
					String location = element[7];
					String magnitudeString = element[8];
					String significanceString = element[9];
					String distanceString = element[10];
					String depthString = element[11];
					int year = Integer.parseInt(yearString);
					int month = Integer.parseInt(monthString);
					int day = Integer.parseInt(dayString);
					int hour = Integer.parseInt(hourString);
					int minute = Integer.parseInt(minuteString);
					int seconds = Integer.parseInt(secondsString);
					double magnitude = Double.parseDouble(magnitudeString);
					int significance = Integer.parseInt(significanceString);
					double distance = Double.parseDouble(distanceString);
					double depth = Double.parseDouble(depthString);
					
					LocalDateTime time = LocalDateTime.of(year, month, day, hour, minute, seconds);
					Earthquake quake = new Earthquake(time, location, magnitude, significance, distance, depth);
					Station station = network.findStation(stationName);
					
					if (station != null) {
						network.addEarthquake(stationName, quake);
					} else {
						network.addStation(stationName);
						network.addEarthquake(stationName, quake);
					}
					stations.add(stationName);
					lineNumber++;
				} catch (Exception exception) {
					System.err.println("Error reading file, line " + lineNumber + ": " + exception.getMessage()
										+ ": " + lineContent);
				}
			}
		}
		return stations;
	}
}
