package edu.westga.cs1302.nss.test.seismic_data;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nss.model.Earthquake;
import edu.westga.cs1302.nss.model.SeismicData;

/**
 * Ensures correct functionality of getEarthquakesMatchingLocation() method.
 * @author Angel Hernandez
 * @version 9/12/2021
 *
 */
public class TestGetEarthquakesMatchingLocation {

	@Test
	public void testNullSearchTerm() {
		SeismicData data = new SeismicData();
		assertThrows(IllegalArgumentException.class, 
					 () ->{
					 data.getEarthquakesMatchingLocation(null);
					 });
	}
	
	@Test
	public void testSearchEmptyList() {
		SeismicData data = new SeismicData();
		ArrayList<Earthquake> result = data.getEarthquakesMatchingLocation("mexico city");
		assertEquals(0, result.size());
	}
	
	@Test
	public void testEmptySearchTerm() {
		SeismicData data = new SeismicData();
		ArrayList<Earthquake> result = data.getEarthquakesMatchingLocation("");
		assertEquals(0, result.size());
	}
	
	@Test
	public void testSearchTermWithOneMatchingLocation() {
		LocalDateTime time1 = LocalDateTime.of(2021, 9, 8, 5, 4);
		SeismicData data = new SeismicData();
		Earthquake firstQuake = new Earthquake(time1, "Mexico City", 10.0, 0, 0.0, 0.0);
		data.add(firstQuake);
		ArrayList<Earthquake> result = data.getEarthquakesMatchingLocation("Mexico City");
		assertEquals(firstQuake, result.get(0));
	}
	
	@Test
	public void testMultipleQuakesOneMatches() {
		LocalDateTime time1 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time2 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time3 = LocalDateTime.of(2021, 9, 8, 5, 4);
		SeismicData data = new SeismicData();
		Earthquake firstQuake = new Earthquake(time1, "Mexico City", 10.0, 0, 0.0, 0.0);
		Earthquake secondQuake = new Earthquake(time2, "Guatemala", 5.0, 300, 2.0, 3.0);
		Earthquake thirdQuake = new Earthquake(time3, "San Jose", 9.0, 300, 2.0, 3.0);
		data.add(firstQuake);
		data.add(secondQuake);
		data.add(thirdQuake);
		ArrayList<Earthquake> result = data.getEarthquakesMatchingLocation("Mexico City");
		assertEquals(firstQuake, result.get(0));
	}
	
	@Test
	public void testMultipleQuakesAllMatches() {
		LocalDateTime time1 = LocalDateTime.of(2020, 9, 8, 5, 4);
		LocalDateTime time2 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time3 = LocalDateTime.of(2020, 10, 8, 5, 4);
		SeismicData data = new SeismicData();
		Earthquake firstQuake = new Earthquake(time1, "Mexico City", 10.0, 0, 0.0, 0.0);
		Earthquake secondQuake = new Earthquake(time2, "Mexico City", 5.0, 300, 2.0, 3.0);
		Earthquake thirdQuake = new Earthquake(time3, "Mexico City", 9.0, 300, 2.0, 3.0);
		data.add(firstQuake);
		data.add(secondQuake);
		data.add(thirdQuake);
		ArrayList<Earthquake> result = data.getEarthquakesMatchingLocation("Mexico City");
		assertEquals(3, result.size());
	}

}
