package edu.westga.cs1302.nss.test.seismic_data;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nss.model.Earthquake;
import edu.westga.cs1302.nss.model.SeismicData;

/**
 * Ensures correct functionality of getHighestSignificanse() method.
 * 
 * @author Angel Hernandez
 * @version 9/12/2021
 *
 */
public class TestGetHighestSignificance {

	@Test
	public void testAnEmptyList() {
		SeismicData data = new SeismicData();
		assertEquals(-1, data.getHighestSignificance());
	}

	@Test
	public void testOneOnTheList() {
		LocalDateTime time1 = LocalDateTime.of(2021, 9, 8, 5, 4);
		SeismicData data = new SeismicData();
		Earthquake firstQuake = new Earthquake(time1, "Mexico City", 10.0, 3000, 0.0, 0.0);
		data.add(firstQuake);
		
		assertEquals(3000, data.getHighestSignificance());
	}
	
	@Test
	public void testMultipleFirstIsHighest() {
		LocalDateTime time1 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time2 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time3 = LocalDateTime.of(2021, 9, 8, 5, 4);
		SeismicData data = new SeismicData();
		Earthquake firstQuake = new Earthquake(time1, "Mexico City", 10.0, 2999, 0.0, 0.0);
		Earthquake secondQuake = new Earthquake(time2, "Guatemala", 5.0, 300, 2.0, 3.0);
		Earthquake thirdQuake = new Earthquake(time3, "San Jose", 9.0, 2500, 2.0, 3.0);
		data.add(firstQuake);
		data.add(secondQuake);
		data.add(thirdQuake);
		assertEquals(2999, data.getHighestSignificance());
	}
	
	@Test
	public void testMultipleMiddleIsHighest() {
		LocalDateTime time1 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time2 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time3 = LocalDateTime.of(2021, 9, 8, 5, 4);
		SeismicData data = new SeismicData();
		Earthquake firstQuake = new Earthquake(time1, "Mexico City", 3.12, 2999, 0.0, 0.0);
		Earthquake secondQuake = new Earthquake(time2, "Guatemala", 9.8, 3000, 2.0, 3.0);
		Earthquake thirdQuake = new Earthquake(time3, "San Jose", 4.05, 300, 2.0, 3.0);
		data.add(firstQuake);
		data.add(secondQuake);
		data.add(thirdQuake);
		assertEquals(3000, data.getHighestSignificance());
	}
	@Test
	public void testMultipleLastIsHighest() {
		LocalDateTime time1 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time2 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time3 = LocalDateTime.of(2021, 9, 8, 5, 4);
		SeismicData data = new SeismicData();
		Earthquake firstQuake = new Earthquake(time1, "Mexico City", 5.11, 500, 0.0, 0.0);
		Earthquake secondQuake = new Earthquake(time2, "Guatemala", 2.23, 1000, 2.0, 3.0);
		Earthquake thirdQuake = new Earthquake(time3, "San Jose", 10.0, 1500, 2.0, 3.0);
		data.add(firstQuake);
		data.add(secondQuake);
		data.add(thirdQuake);
		assertEquals(1500, data.getHighestSignificance());	
	}

}
