package edu.westga.cs1302.nss.test.seismic_data;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nss.model.Earthquake;
import edu.westga.cs1302.nss.model.SeismicData;

/**
 * Ensures correct functionality of the add method.
 * 
 * @author Angel Hernandez
 * @version 9/09/2021
 *
 */
public class TestAdd {

	@Test
	public void testAddNullEarthquake() {
		SeismicData data = new SeismicData();
		assertThrows(IllegalArgumentException.class, () -> data.add(null));
		assertEquals(0, data.size());
	}

	@Test
	public void testAddToAnEmptyList() {
		LocalDateTime time = LocalDateTime.of(2021, 9, 8, 5, 4);
		SeismicData data = new SeismicData();
		Earthquake oneQuake = new Earthquake(time, "Mexico City", 0.01, 0, 0.0, 0.0);
		data.add(oneQuake);
		
		assertEquals(1, data.size());
		assertEquals(oneQuake, data.getEarthquakes().get(0));
	}
	
	@Test
	public void testAddMultipleEarthquakes() {
		LocalDateTime time1 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time2 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time3 = LocalDateTime.of(2021, 9, 8, 5, 4);
		SeismicData data = new SeismicData();
		Earthquake firstQuake = new Earthquake(time1, "Mexico City", 0.01, 0, 0.0, 0.0);
		Earthquake secondQuake = new Earthquake(time2, "Guatemala", 0.05, 300, 2.0, 3.0);
		Earthquake thirdQuake = new Earthquake(time3, "San Jose", 0.05, 300, 2.0, 3.0);
		data.add(firstQuake);
		data.add(secondQuake);
		data.add(thirdQuake);
		
		assertEquals(3, data.size());
		assertEquals(firstQuake, data.getEarthquakes().get(0));
		assertEquals(secondQuake, data.getEarthquakes().get(1));
		assertEquals(thirdQuake, data.getEarthquakes().get(2));
		
	}
	
	@Test
	public void testEarthquakeAlreadyOnList() {
		LocalDateTime time = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time2 = LocalDateTime.of(2021, 9, 8, 5, 4);
		LocalDateTime time3 = LocalDateTime.of(2021, 9, 8, 5, 4);
		SeismicData data = new SeismicData();
		Earthquake oneQuake = new Earthquake(time, "Mexico City", 0.01, 0, 0.0, 0.0);
		Earthquake secondQuake = new Earthquake(time2, "Mexico City", 0.05, 300, 2.0, 3.0);
		Earthquake thirdQuake = new Earthquake(time3, "San Jose", 0.05, 300, 2.0, 3.0);
		assertTrue(data.add(oneQuake));
		assertThrows(IllegalArgumentException.class, () -> data.add(secondQuake));
		assertTrue(data.add(thirdQuake));
		
		assertEquals(2, data.size());
		assertEquals(oneQuake, data.getEarthquakes().get(0));
		assertEquals(thirdQuake, data.getEarthquakes().get(1));
	}
}
